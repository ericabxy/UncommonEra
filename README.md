Uncommon Era is a libre culture shared universe that you can use as a 
setting to write your short story, novel, video game, RPG, or any other 
media you can think of. The only requirement is that you follow the 
terms of the Creative Commons license(s) used by the project.

To get started read Background.txt, Locations.txt, Travel.txt, and 
Devices.txt, then read everything else that seems interesting.
